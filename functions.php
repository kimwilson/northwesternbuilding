<?php

/**
 * Enqueue scripts and styles.
 */
function ekcetera_scripts() {
	/* Add Custom CSS - normalize, foundation, and custom */
	wp_enqueue_style( 'ekcetera-custom-style', get_stylesheet_directory_uri() . '/styles/custom.min.css', array(), '1' );

	/* Add Foundation JS */
	wp_enqueue_script( 'foundation-js', get_stylesheet_directory_uri() . '/js/libs/foundation.min.js', array( 'jquery' ), '1', true );
	wp_enqueue_script( 'foundation-modernizr-js', get_stylesheet_directory_uri() . '/js/libs/vendor/modernizr.js', array( 'jquery' ), '1', true );
 
	/* Foundation Init JS */
	wp_enqueue_script( 'ekcetera-custom-js', get_stylesheet_directory_uri() . '/js/dist/production.min.js', array(), '1', true );

	wp_enqueue_script( 'ekcetera-navigation', get_stylesheet_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'ekcetera-skip-link-focus-fix', get_stylesheet_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

}
add_action( 'wp_enqueue_scripts', 'ekcetera_scripts' );

/* removes the old non-responsive slider from parent theme */
add_action( 'after_setup_theme','remove_project_custom_init', 100 );

function remove_project_custom_init() {
    remove_action( 'init', 'post_type_slides');
}



?>