<?php get_header(); ?>

<div class="row">
<div id="home-container">

	<!--slideshow-->
	
	<div id="slider-wrapper">
	   <?php masterslider(1); ?>
  		
	</div> <!--slideshow end-->

</div><!--home container end-->

<div class="clear"></div>

<!--welcome-->
	<div id="welcome_container">

		<div id="welcome-box">
		
	<h1><?php if(get_option('bz_head_01') != NULL){ echo get_option('bz_head_01');} else echo "write your headline here" ?></h1>
		
	</div>

</div><!--welcome end-->

<div class="clear"></div>

<div id="home-boxes" class="row"><!--home icon boxes---------------------------------->
			
				<?php $i = 1; ?>
				
		<?php query_posts('post_type=home_boxes');

		while ( have_posts() ) : the_post(); ?>
		
			<?php $last_class = '';
				if(($i) % 3 == 0)
					{	
					$last_class = ' last';
						}
				?>
					<!--column --><div class="small-12 medium-4 columns">
					<!--boxes --><div class="<?php echo $last_class; ?>" style="margin-bottom:40px;padding:0 0 0 10px;">
		
				<?php $icon_small='';
	$icon_small= get_post_meta($post->ID, 'Icon', true);?>
	
				<!--box icon--><div class="box-icon">
						
						<?php if($icon_small<>'') { ?>
						
						<img src="<?php bloginfo('template_directory'); ?>/timthumb.php?src=<?php echo get_post_meta($post->ID, 'Icon', true); ?>&h=43&w=43&zc=1" alt="" />
						
						<?php } ?>
					
					</div> <!--box-icon close-->
					
		<div class="iconbox-content">			
	
			<h3>
			<?php if(get_post_meta($post->ID, 'TitleLink', true)): ?>
				<a href="<?php echo get_post_meta($post->ID, 'TitleLink', true); ?>">
				<?php small_title(33); ?>
				</a>
			<?php else: ?>
				<?php small_title(33); ?>
			<?php endif; ?>
			</h3>
			<p><?php the_content(); ?></p>
	
		</div> <!--box-content close-->
				
				</div><!--boxes  end-->
				</div><!-- end column -->
				<?php $i++; ?>
				
		<?php endwhile; wp_reset_query(); ?>
			
</div><!--home boxes end-->
		
		
			
		<div class="clear"></div>
				
<!--carousel-->
<!--
 <?php $show_carousel = get_option('bz_show_caro'); 
if($show_carousel != 'no') {
?>
	<div id="jcarousel"><ul id="mycarousel" class="jcarousel-skin-ie7">
	
	<?php query_posts('category_name=' . get_option('bz_caro_cat')); ?>

<?php while (have_posts()) : the_post(); ?>

			<li><a href="<?php the_permalink() ?>"><img src="<?php bloginfo('template_directory'); ?>/timthumb.php?src=<?php echo get_post_meta($post->ID, 'Thumbnail', true); ?>&h=60&w=160&zc=1" title="<?php the_title(); ?>" class="tooltip" alt="" />
			</a></li>
		
<?php endwhile; wp_reset_query(); ?>
	
	</ul></div>
	
	<div class="clear"></div> 	 <?php } ?>
	-->
	
	<div id="footer_stripe">
		<img src="http://0365eda.netsolhost.com/wordpress/wp-content/uploads/2012/10/thefooter.jpg" alt="nw_footer" width="100%"  />
	</div>	
</div>
</div>
<!--wrapper end-->
</div> <!-- foundation row end -->

<?php get_footer(); ?>