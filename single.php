<?php get_header(); ?>
	
		<!--sub head container--><div id="subhead_container">

			<div id="subhead">
		
<h1><?php the_title(); ?></h1>
			
			</div>
			
		</div>
		

	<!--content-->
<div id="content_container">
	
	<div id="content">
		<div class="row">
			<div class="small-12 medium-8 columns">
			<div id="left-col">

				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				
				
				<?php $thumb_small='';
					  $thumb_small= get_post_meta($post->ID, 'Thumbnail', true);?>

				<div class="post-entry <?php if ($thumb_small <> '') {echo "timbg";} ?>">
				
					<div class="clear"></div>
					<?php if($thumb_small<>'') { ?>
				
						<a href="<?php the_permalink() ?>"><img class="alignleft" src="<?php bloginfo('template_directory'); ?>/timthumb.php?src=<?php echo get_post_meta($post->ID, 'Thumbnail', true); ?>&h=200&w=600&zc=1" alt="" /></a>
				
					<?php } ?>

					<?php the_content( __( '', 'buziness' ) ); ?>
					<div class="clear"></div>

					<?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'buziness' ), 'after' => '' ) ); ?>
								
					<?php the_tags('Social tagging: ',' > '); ?>
							
				</div><!--post-entry end-->

				<?php endwhile; ?>

			</div> <!--left-col end-->
			</div>
			<div class="small-12 medium-4 columns">
				<?php get_sidebar(); ?>
			</div>

        </div>
	</div> 
</div>
<!--content end-->
	
</div>
<!--wrapper end-->

<?php get_footer(); ?>