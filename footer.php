	<!--footer-->
	<div class="clear"></div>
		<div class="row">
		<div id="footer">
		
	<!--footer container--><div id="footer-container">
		
		<div id="footer-widget">
			
			<?php
			/* A sidebar in the footer? Yep. You can can customize
			 * your footer with four columns of widgets.
			 */
			get_sidebar( 'footer' );
			?>
			
			</div><!--footer widget end-->
			
		</div><!-- footer container-->
		
		<div class="clear"></div>		
	
		<div id="footer-info">		
			<div class="small-6 columns text-left">
			<img src="<?php bloginfo('template_directory'); ?>/images/managed_by_hbg.jpg" alt="Professionally managed by HBG" title="Professionally managed by HBG" />
			</div>
			<div class="small-6 columns text-right">
			<img src="<?php bloginfo('template_directory'); ?>/images/pak_properties.jpg" alt="PAK Properties" title="PAK Properties" />
			</div>
					
		</div><!--footer info end-->
			
		<div class="clear"></div>
	
		</div><!--footer end-->
		</div>
		<?php wp_footer(); ?>

</body>

</html>