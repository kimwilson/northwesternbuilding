<?php /* Template Name: Full Width
*/ ?> 

<?php get_header(); ?>

		<div id="subhead_container">
		
			<div id="subhead">
		
		<h1><?php the_title(); ?></h1>
			
			</div>
			
		</div>
		
	<div id="content_container">

	<div id="post-entry-fullwidth">

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		
		<?php if(get_post_meta($post->ID, 'Thumbnail', true)) : ?>
			<img class="sub_hero" src="<?php bloginfo('template_directory'); ?>/timthumb.php?src=<?php echo get_post_meta($post->ID, 'Thumbnail', true); ?>&h=400&w=1200&zc=1" alt="<?php the_title(); ?>" />
		<?php endif; ?>
		<?php the_content(); ?>
		<div class="clear"></div>
		<?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'buziness' ), 'after' => '' ) ); ?>
			
	<?php endwhile; ?>

	</div> 
</div>
<!--full width end-->

		
</div>
<!--wrapper end-->

<?php get_footer(); ?>