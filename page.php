<?php get_header(); ?>
		
		<div id="subhead_container">
		
			<div id="subhead">
		
				<h1><?php the_title(); ?></h1>
			
			</div>
			
		</div>

		<!--content-->
		<div id="content_container">
			
			<div id="content">
				<div id="hero_wrap">
				<img class="sub_hero" src="<?php bloginfo('template_directory'); ?>/timthumb.php?src=<?php echo get_post_meta($post->ID, 'Thumbnail', true); ?>&h=400&w=1200&zc=1" alt="<?php the_title(); ?>" />
				</div>
				
				<div class="row">
					<div id="left-col" class="small-12 medium-6 large-8 columns">

						<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
							
							<div class="post-entry">

								<?php the_content(); ?>
								<div class="clear"></div>
								<?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'buziness' ), 'after' => '' ) ); ?>
								
							</div><!--post-entry end-->


						<?php endwhile; ?>
					</div> <!--left-col end-->
					<div class="small-12 medium-6 large-4 columns">
					<?php get_sidebar(); ?>
					</div>
				</div>
			</div> 
		</div>
		<!--content end-->
	
</div>
<!--wrapper end-->
<?php get_footer(); ?>