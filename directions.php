<?php /* Template Name: Directions
*/ ?> 

<?php get_header(); ?>

		<div id="subhead_container">
		
			<div id="subhead">
		
		<h1><?php the_title(); ?></h1>
			
			</div>
			
		</div>
		
	<div id="content_container">

	<div id="post-entry-fullwidth">

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		
		<?php if(get_post_meta($post->ID, 'Thumbnail', true)) : ?>
			<iframe style="margin-bottom: 20px;" width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=275+4th+St+E,+St+Paul,+MN&amp;aq=0&amp;oq=275+4th+St+E,+stpaul&amp;sll=44.949058,-93.085289&amp;sspn=0.001335,0.00284&amp;ie=UTF8&amp;hq=&amp;hnear=275+4th+St+E,+St+Paul,+Minnesota+55101&amp;t=m&amp;ll=44.949037,-93.091235&amp;spn=0.01063,0.039482&amp;z=15&amp;output=embed&iwloc=near"></iframe>
		<?php endif; ?>
		<?php the_content(); ?>
		<div class="clear"></div>
		<?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'buziness' ), 'after' => '' ) ); ?>
			
	<?php endwhile; ?>

	</div> 
</div>
<!--full width end-->

		
</div>
<!--wrapper end-->

<?php get_footer(); ?>