<?php get_header(); ?>

		<div id="subhead_container">

			<div id="subhead">
		
		<h1><?php if ( is_category() ) {
		single_cat_title();
	} elseif (is_archive() ) {
		echo 'Archives for '; single_month_title();
	} else {
		wp_title('',true);
	} ?></h1>
			
			</div>
			
		</div>

		<!--content-->
		<div id="content_container">	
			<div id="content">
				<div class="row">
				<div class="small-12 medium-8 columns">
					<div id="left-col">
				
						<?php get_template_part( 'loop', 'index' ); ?>

					</div> <!--left-col end-->
                </div>
                <div class="small-12 medium-4 columns">
					<?php get_sidebar(); ?>
				</div>

				</div>
			</div> 
		</div>
		<!--content end-->
	
</div>
<!--wrapper end-->

<?php get_footer(); ?>