'use strict';

module.exports = function(grunt) {
  grunt.config.set('uglify', {
    prod: {
    	options: {
	      compress: {
	        drop_console: true
	      }
	    },
      src: 'js/dist/production.js',
      dest: 'js/dist/production.min.js',
    },
    dev: {
    	options: {
	      beautify: {
	        beautify: true
	      }
	    },
      src: 'js/dist/production.js',
      dest: 'js/dist/production.min.js',
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
};
