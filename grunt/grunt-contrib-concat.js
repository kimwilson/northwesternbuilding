'use strict';

module.exports = function(grunt) {
  grunt.config.set('concat', {
    dev: {
        src: ['js/src/**/*.js'], // All JS in the src folder
        dest: 'js/dist/production.js'
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
};
