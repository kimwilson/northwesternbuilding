'use strict';

module.exports = function(grunt) {
  grunt.config.set('watch', {
    options: {
        livereload: true,
    },
    css: {
      files: ['styles/**/*.scss','styles/**/*.css'],
      tasks: ['sass:dev','cssmin:dev'],
      options: {
        spawn: false,
      }
    },
    jshintrc: {
      files: ['**/.jshintrc'],
      tasks: ['jshint'],
      options: {
        spawn: false,
      }
    },
    dev: {
      files: ['<%= jshint.dev.src %>'],
      tasks: ['jshint:dev','concat','uglify:dev'],
      options: {
        spawn: false,
      }
    },
    scripts: {
      files: ['<%= jshint.prod.src %>'],
      tasks: ['jshint:prod'],
      options: {
        spawn: false,
      } 
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
}; 