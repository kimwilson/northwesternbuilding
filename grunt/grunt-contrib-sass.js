'use strict';

module.exports = function(grunt) {
  grunt.config.set('sass', {
    options: {
      loadPath: ['bower_components/foundation/scss']
    },
    dev: {                            // Target
      options: {
        style: 'expanded',
        /*debugInfo: true,
        lineNumbers: true*/
      },
      files: {                         // Dictionary of files
        'styles/custom.css': 'styles/custom.scss',       // 'destination': 'source'
      }
    },
    prod: {
      options: {
        style: 'compressed'
      },
      files: {                         // Dictionary of files
        'styles/custom.css': 'styles/custom.scss',       // 'destination': 'source'
      }
    },
    foundation: {
      options: {
        style: 'compressed'
      },
      files: {                         // Dictionary of files
        'bower_components/foundation/css/foundation.css': 'bower_components/foundation/scss/foundation.scss',       // 'destination': 'source'
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-sass');
};
