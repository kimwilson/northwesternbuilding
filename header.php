<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>


	<head profile="http://gmpg.org/xfn/11">
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
		<meta name=viewport content="width=device-width, initial-scale=1">
		<title><?php if (is_front_page() ) {
    bloginfo('name');
	} elseif ( is_category() ) {
		single_cat_title(); echo ' - ' ; bloginfo('name');
	} elseif (is_single() ) {
		single_post_title();
	} elseif (is_page() ) {
		single_post_title(); echo ' - '; bloginfo('name');
	} elseif (is_archive() ) {
		single_month_title(); echo ' - '; bloginfo('name');
	} else {
		wp_title('',true);
	} ?></title>
			
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	
	<?php wp_enqueue_style('superfish', get_template_directory_uri()  . '/css/superfish.css'); ?>
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/tipsy.css" type="text/css" />
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/nivo-slider.css" type="text/css" />
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/skin.css" type="text/css" />	
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<?php if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' ); ?>

<?php //wp_enqueue_script('jquery'); ?>
	
	<?php wp_head(); ?>
	
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.jcarousel.min.js"></script>	
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.tipsy.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.nivo.slider.pack.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/tabs.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/superfish.js"></script>

<!-- Javascript Start //-->

		<script type="text/javascript"> 
 
    jQuery(document).ready(function() { 
        jQuery('ul.nav').superfish({ 
            delay:       200,                            // one second delay on mouseout 
            speed:       'medium',                          // faster animation speed 
            autoArrows:  false,                           // disable generation of arrow mark-up 
            dropShadows: false                            // disable drop shadows 
        }); 
    });
 
</script>

<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery('#mycarousel').jcarousel({

    });
});
</script>


<script type="text/javascript">
jQuery(document).ready(function() {
       jQuery('#slider').nivoSlider({
	   animSpeed:300, //Slide transition speed
        pauseTime:5000,	
		directionNav:true,
		pauseOnHover:false,
		directionNavHide:false,
		effect:'fade'	
		});
    });
</script>

<script type='text/javascript'>
 jQuery(document).ready(function() {
    
    jQuery('.tooltip').tipsy({fade: true});
    
  });
</script>

<script type="text/javascript">
jQuery(function() { 

jQuery("#accordion").tabs("#accordion div.pane", {tabs: 'h2', effect: 'slide'});
});
		</script>

<script type="text/javascript">		
jQuery(function() {
	// setup ul.tabs to work as tabs for each div directly under div.panes
	jQuery("ul.tabs").tabs("div.panes > div", {tabs: 'h2', effect: 'fade'});

});	
</script>	

<script type="text/javascript" charset="utf-8">
		jQuery(document).ready(function(){
			jQuery("a[rel^='prettyPhoto']").prettyPhoto({theme: 'light_rounded'});
		});
	</script>
	
	<script type="text/javascript">
	jQuery(document).ready(function(){

jQuery(".pphoto a, .pphoto2 a").append("<span></span>");

// ON MOUSE OVER
jQuery(".pphoto img, .pphoto2 img, .box-thumb").hover(function () {

// SET OPACITY TO 100%
jQuery(this).stop().animate({
opacity: 0.5
}, "fast");
},
	 
// ON MOUSE OUT
function () {
	 
// SET OPACITY BACK TO 50%
jQuery(this).stop().animate({
opacity: 1.0
}, "slow");
});
});

</script>


<!--[if IE 6]>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/ie6.css" />
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/DD_belatedPNG_0.0.8a.js"></script>
<script type="text/javascript">

DD_belatedPNG.fix('.navbar, #logo img, #searchsubmit, #img-left img, #img-right img, .widget-container ul li, .timbg, .nivo-controlNav a');

</script>
<![endif]-->

	<?php $googleanalytics=get_option("bz_go_code"); ?>
	<?php echo stripslashes($googleanalytics); ?>
	
<!-- Google Analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36180505-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
	
</head>

<body <?php body_class(); ?>>

	<!--wrapper-->
	<div class="row"> <!-- add row for foundation -->
	<div id="wrapper">
	
	<!--headercontainer-->
	<div id="header_container" class="row">
	
		<!--header-->
		<div class="small-12 medium-6 large-4 columns">

				<?php if ( ( get_option('bz_logo_01') ) != '' ) { ?>
					<div id="logo2"><a href="<?php echo home_url(); ?>" title="<?php bloginfo('description'); ?>"><img src="<?php echo get_option('bz_logo_01'); ?>" alt="<?php echo get_option('bz_logo_02'); ?>" /></a></div><!--logo end-->
				<?php } else { ?>
						<div id="logo"><a href="<?php echo home_url(); ?>" title="<?php bloginfo('description'); ?>"><?php bloginfo( 'name' ); ?></a></div><!--logo end-->
				<?php } ?>
			
		</div>	
			
		<div id="menubar" class="small-12 medium-6 large-8 columns">
			
			<?php $navcheck = '' ; ?>
			
				<?php if (function_exists( 'wp_nav_menu' )) {
					$navcheck = wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary', 'menu_class' => 'nav' ,'fallback_cb' => '', 'echo' => false ) );
				} ?>

				<?php  if ($navcheck == '') { ?>
				
				<ul class="nav">
					<li class="current"><a href="<?php echo home_url(); ?>" title="Home">Home</a></li>
								
					<?php
							if (get_option('bz_menu_bar') <> ''){
								$menubar = implode(",", get_option('bz_menu_bar'));
								}
							if($menubar){
								wp_list_pages('title_li=&include='.$menubar);
							}
					?>
					
					<?php
							if (get_option('bz_cat_bar') <> ''){
								$catbar = implode(",", get_option('bz_cat_bar'));
								}
							if($catbar){
								wp_list_categories('title_li=&include='.$catbar);
							}
					?>

				</ul>
			<?php } else echo($navcheck); ?> 

		</div>
	
	<!--menu end---------------------------------------------------------->
			
			<div class="clear"></div>
		
		
	</div><!--header container end-->	
		
